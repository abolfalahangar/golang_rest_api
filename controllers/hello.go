package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type HelloWorldController struct{}

// GET /books
// Get all books
func (h *HelloWorldController) Default(c *gin.Context) {

	c.JSON(http.StatusOK, gin.H{"message": "Hello world, climate change is real"})
}
