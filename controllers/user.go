package controllers

import (
	"crud/forms"
	"crud/helpers"
	"crud/models"
	"crud/services"

	"github.com/gin-gonic/gin"
)

var userModel = new(models.UserModel)

type UserController struct{}

func (u *UserController) SignUp(c *gin.Context) {
	var data forms.SignupUserCommand

	result, _ := userModel.GetUserByEmail(data.Email)

	if result.Email != "" {
		c.JSON(403, gin.H{"message": "Email is already in use"})
		c.Abort()
		return
	}
	if c.BindJSON(&data) != nil {
		// specified response
		c.JSON(406, gin.H{"message": "Provide relevant fields"})
		// abort the request
		c.Abort()
		// return nothing
		return
	}
	err := userModel.SignUp(data)

	if err != nil {
		c.JSON(400, gin.H{"message": "Problem creating an account"})
		c.Abort()
		return
	}

	c.JSON(201, gin.H{"message": "New user account registered"})
}

func (u *UserController) ResetLink(c *gin.Context) {
	// Defined schema for the request body
	var data forms.ResendCommand

	// Ensure the user provides all values from the request.body
	if (c.BindJSON(&data)) != nil {
		// Return 400 status if they don't provide the email
		c.JSON(400, gin.H{"message": "Provided all fields"})
		c.Abort()
		return
	}

	// Fetch the account from the database based on the email
	// provided
	result, err := userModel.GetUserByEmail(data.Email)

	// Return 404 status if an account was not found
	if result.Email == "" {
		c.JSON(404, gin.H{"message": "User account was not found"})
		c.Abort()
		return
	}

	// Return 500 status if something went wrong while fetching
	// account
	if err != nil {
		c.JSON(500, gin.H{"message": "Something wrong happened, try again later"})
		c.Abort()
		return
	}

	// Generate the token that will be used to reset the password
	resetToken, _ := services.GenerateNonAuthToken(result.Email)

	// The link to be clicked in order to perform a password reset
	link := "http://localhost:5000/api/v1/password-reset?reset_token=" + resetToken
	// Define the body of the email
	body := "Here is your reset <a href='" + link + "'>link</a>"
	html := "<strong>" + body + "</strong>"

	// Initialize email sendout
	email := services.SendMail("Reset Password", body, result.Email, html, result.Name)

	// If email was sent, return 200 status code
	if email == true {
		c.JSON(200, gin.H{"messsage": "Check mail"})
		c.Abort()
		return
		// Return 500 status when something wrong happened
	} else {
		c.JSON(500, gin.H{"message": "An issue occured sending you an email"})
		c.Abort()
		return
	}
}

func (u *UserController) PasswordReset(c *gin.Context) {
	var data forms.PasswordResetCommand

	// Ensure they provide data based on the schema
	if c.BindJSON(&data) != nil {
		c.JSON(406, gin.H{"message": "Provide relevant fields"})
		c.Abort()
		return
	}

	// Ensures that the password provided matches the confirm
	if data.Password != data.Confirm {
		c.JSON(400, gin.H{"message": "Passwords do not match"})
		c.Abort()
		return
	}

	// Get token from link query sent to your email
	resetToken, _ := c.GetQuery("reset_token")

	// Decode the token
	userID, _ := services.DecodeNonAuthToken(resetToken)

	// Fetch the user
	result, err := userModel.GetUserByEmail(userID)

	if err != nil {
		// Return response when we get an error while fetching user
		c.JSON(500, gin.H{"message": "Something wrong happened, try again later"})
		c.Abort()
		return
	}
	// Check if account exists
	if result.Email == "" {
		c.JSON(404, gin.H{"message": "User accoun was not found"})
		c.Abort()
		return
	}

	// Hash the new password
	newHashedPassword := helpers.GeneratePasswordHash([]byte(data.Password))

	// Update user account
	_err := userModel.UpdateUserPass(userID, newHashedPassword)

	if _err != nil {
		// Return response if we are not able to update user password
		c.JSON(500, gin.H{"message": "Somehting happened while updating your password try again"})
		c.Abort()
		return
	}

	c.JSON(201, gin.H{"message": "Password has been updated, log in"})
	c.Abort()
	return
}

func (u *UserController) VerifyLink(c *gin.Context) {
	var data forms.ResendCommand

	// Ensure they provide all relevant fields in the request body
	if (c.BindJSON(&data)) != nil {
		c.JSON(400, gin.H{"message": "Provided all fields"})
		c.Abort()
		return
	}

	// Fetch account from database
	result, err := userModel.GetUserByEmail(data.Email)

	// Check if account exist return 404 if not
	if result.Email == "" {
		c.JSON(404, gin.H{"message": "User account was not found"})
		c.Abort()
		return
	}

	if err != nil {
		c.JSON(500, gin.H{"message": "Something wrong happened, try again later"})
		c.Abort()
		return
	}

	// Generate token to hold user details
	resetToken, _ := services.GenerateNonAuthToken(result.Email)

	// Define email body
	link := "http://localhost:5000/api/v1/verify-account?verify_token=" + resetToken
	body := "Here is your reset <a href='" + link + "'>link</a>"
	html := "<strong>" + body + "</strong>"

	// Initialize email sendout
	email := services.SendMail("Verify Account", body, result.Email, html, result.Name)

	// If email send 200 status code
	if email == true {
		c.JSON(200, gin.H{"messsage": "Check mail"})
		c.Abort()
		return
	} else {
		c.JSON(500, gin.H{"message": "An issue occured sending you an email"})
		c.Abort()
		return
	}
}

func (u *UserController) RefreshToken(c *gin.Context) {
	// Get refresh token from header
	refreshToken := c.Request.Header["Refreshtoken"]

	// Check if refresh token was provided
	if refreshToken == nil {
		c.JSON(403, gin.H{"message": "No refresh token provided"})
		c.Abort()
		return
	}

	// Decode token to get data
	email, err := services.DecodeRefreshToken(refreshToken[0])

	if err != nil {
		c.JSON(500, gin.H{"message": "Problem refreshing your session"})
		c.Abort()
		return
	}

	// Create new token
	accessToken, _refreshToken, _err := services.GenerateToken(email)

	if _err != nil {
		c.JSON(500, gin.H{"message": "Problem creating new session"})
		c.Abort()
		return
	}

	c.JSON(200, gin.H{"message": "Log in success", "token": accessToken, "refresh_token": _refreshToken})
}
