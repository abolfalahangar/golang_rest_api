package helpers

// Import the URL
import "net/url"

// IsValidURL checks validity of URL
func IsValidURL(link string) bool {
	// Parse the URL with the net library
	_, err := url.ParseRequestURI(link)

	// Check if we got an error while parsing the url
	if err != nil {
		return false
	}

	u, err := url.Parse(link)

	// Check if we had an error while parsing
	// Check if the scheme is provided https:// http://
	// Check if a host exists google.com, bywachira.com
	if err != nil || u.Scheme == "" || u.Host == "" {
		return false
	}

	return true
}
