package main

import (
	"crud/controllers"
	"fmt"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
)

type Login struct {
	User     string `form:"user" json:"user" xml:"user"  binding:"required"`
	Password string `form:"password" json:"password" xml:"password" binding:"required"`
}

type StructA struct {
	FieldA string `form:"field_a"`
}

type StructB struct {
	NestedStruct StructA
	FieldB       string `form:"field_b"`
}

type Product struct {
	Name  string `json:"name"`
	Id    int32  `json:"id"`
	Price int32  `json:"price"`
}

func init() {
	if err := godotenv.Load(); err != nil {
		log.Printf("No .env file found")
	}
}

func main() {
	r := gin.Default()
	r.Use(gin.Logger())

	r.TrustedProxies = []string{"192.168.1.2"}

	r.GET("/", func(c *gin.Context) {

		fmt.Printf("ClientIP: %s\n", c.ClientIP())
		c.JSON(http.StatusOK, gin.H{"data": "hello world"})
	})

	r.GET("/StructA", func(c *gin.Context) {
		var b StructB
		c.Bind(&b)
		c.JSON(200, gin.H{
			"a": b.NestedStruct,
			"b": b.FieldB,
		})
	})
	hello := new(controllers.HelloWorldController)

	r.GET("/hello", hello.Default)

	user := new(controllers.UserController)
	// Create the signup endpoint
	r.POST("/signup", user.SignUp)
	r.PUT("/password-reset", user.PasswordReset)

	r.GET("/Products", func(c *gin.Context) {
		var b Product
		b.Id = 1
		b.Name = "pride"
		b.Price = 12000000

		c.Bind(&b)
		c.JSON(200, gin.H{
			"products": b,
		})
	})

	r.POST("/", func(c *gin.Context) {
		name := c.PostForm("name")
		message := c.PostForm("message")

		fmt.Printf("name: %s; message: %s", name, message)
		c.JSON(http.StatusCreated, gin.H{"data": "ok"})
	})

	r.POST("/login", func(c *gin.Context) {
		var json Login
		if err := c.ShouldBindJSON(&json); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"errorrr": err.Error()})
			return
		}
		fmt.Printf("json: %s;", json)
		if json.User != "ali" || json.Password != "123" {
			c.JSON(http.StatusUnauthorized, gin.H{"status": "unauthorized"})

			return
		}
		c.JSON(http.StatusOK, gin.H{"status": "you are logged in"})
	})

	r.GET("/cookie", func(c *gin.Context) {

		cookie, err := c.Cookie("gin_cookie")

		if err != nil {
			cookie = "NotSet"
			c.SetCookie("gin_cookie", "test", 3600, "/", "localhost", false, true)
		}

		fmt.Printf("Cookie value: %s \n", cookie)
	})

	r.NoRoute(func(c *gin.Context) {
		c.JSON(404, gin.H{"message": "Not found"})
	})

	r.Run()
}
