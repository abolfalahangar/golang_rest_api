package models

import (
	"gopkg.in/mgo.v2/bson"

	"crud/forms"
	"crud/helpers"
)

type User struct {
	ID         bson.ObjectId `json:"_id,omitempty" bson:"_id,omitempty"`
	Name       string        `json:"name" bson:"name"`
	Email      string        `json:"email" bson:"email"`
	Password   string        `json:"password" bson:"password"`
	IsVerified bool          `json:"is_verified" bson:"is_verified"`
}

type UserModel struct{}

func (u *UserModel) SignUp(data forms.SignupUserCommand) error {
	collection := dbConnect.Use(databaseName, "user")

	err := collection.Insert(bson.M{
		"name":     data.Name,
		"email":    data.Email,
		"password": helpers.GeneratePasswordHash([]byte(data.Password)),
		// This will come later when adding verification
		"is_verified": false,
	})

	return err
}

func (u *UserModel) GetUserByEmail(email string) (user User, err error) {
	// Connect to the user collection
	collection := dbConnect.Use(databaseName, "user")
	// Assign result to error object while saving user
	err = collection.Find(bson.M{"email": email}).One(&user)
	return user, err
}

func (u *UserModel) GetUserById(id string) (user User, err error) {
	collection := dbConnect.Use(databaseName, "user")

	err = collection.Find(bson.M{"_id": id}).One(&user)

	return user, err
}

func (u *UserModel) UpdateUserPass(email string, password string) (err error) {
	collection := dbConnect.Use(databaseName, "user")
	err = collection.Update(bson.M{"email": email}, bson.M{"$set": bson.M{"password": password}})

	return err
}

func (u *UserModel) VerifyAccount(email string) (err error) {

	collection := dbConnect.Use(databaseName, "user")
	err = collection.Update(bson.M{"email": email}, bson.M{"$set": bson.M{"is_verified": true}})
	return err
}
