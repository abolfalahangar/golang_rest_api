package services

import (
	// will allow us to make calls to the website
	"net/http"
	// help in defining a timeout for the request
	"time"

	// Help in finding tags and attributes in the html loaded
	"github.com/PuerkitoBio/goquery"
	// To handle failed responses
	"github.com/gin-gonic/gin"
)

// Scrapper holds methods to scrape a site
type Scrapper struct{}

// Meta define meta data type
type Meta struct {
	Image, Description, URL, Title, Site string
}

// CallWebsite make an http request to a website
func (scrapper *Scrapper) CallWebsite(websiteURL string, c *gin.Context) Meta {
	// Define variable to hold the attributes srapped
	var meta Meta = Meta{
		Image:       "",
		Description: "",
		URL:         "",
		Title:       "",
		Site:        "",
	}

	// Define the client timeout
	client := &http.Client{
		// Set timeout to abort if the request takes too long
		Timeout: 30 * time.Second,
	}

	// Define the request obviously it is a GET one
	request, err := http.NewRequest("GET", websiteURL, nil)

	// Check if we get an error
	if err != nil {
		// Respond with a 500 status code
		c.AbortWithStatusJSON(500, gin.H{"message": err})
	}

	// Set headers
	request.Header.Set("pragma", "no-cache")

	request.Header.Set("cache-control", "no-cache")

	request.Header.Set("dnt", "1")

	request.Header.Set("upgrade-insecure-requests", "1")

	request.Header.Set("referer", websiteURL)

	// Make website request call
	resp, err := client.Do(request)

	// If we have a successful request
	if resp.StatusCode == 200 {
		doc, err := goquery.NewDocumentFromReader(resp.Body)

		if err != nil {
			c.AbortWithStatusJSON(400, gin.H{"message": err})
		}

		// Map through all meta tags fetched
		doc.Find("meta").Each(func(i int, s *goquery.Selection) {
			// Specify the meta tags we want and assign the variables as the doc.Find
			// loops through all of them
			metaProperty, _ := s.Attr("property")
			metaContent, _ := s.Attr("content")

			// If we happen to get any of the two then assign the Site attribute for Meta
			if metaProperty == "og:site_name" || metaProperty == "twitter:site" {
				meta.Site = metaContent
			}

			// If we happen to get any of the two then assign the URL attribute for Meta
			if metaProperty == "og:url" {
				meta.URL = metaContent
			}
			// If we happen to get any of the two then assign the Image attribute for Meta
			if metaProperty == "og:image" || metaProperty == "twitter:image" {
				meta.Image = metaContent
			}

			// If we happen to get any of the two then assign the Title attribute for Meta
			if metaProperty == "og:title" || metaProperty == "twitter:title" {
				meta.Title = metaContent
			}

			// If we happen to get any of the two then assign the Description attribute for Meta
			if metaProperty == "og:description" || metaProperty == "twitter:description" {
				meta.Description = metaContent
			}
		})
	}

	// Return the meta variable with updated fields
	return meta
}
